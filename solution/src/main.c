#include "bmp.h"
#include "image.h"
#include <stdio.h>
#include <stdlib.h>

#define SOURCE_IMAGE_INDEX 1
#define DESTINATION_IMAGE_INDEX 2
#define ANGLE_INDEX 3
#define REQUIRED_ARG_COUNT 4

#define MAX_ANGLE 270
#define MIDDLE_ANGLE 0
#define MIN_ANGLE (-270)

int main(int argc, char* argv[]) {
    if (argc != REQUIRED_ARG_COUNT) {
        fprintf(stderr, "Usage: %s <source-image> <destination-image> <angle>\n", argv[0]);
        return READ_ARG_ERROR;
    }

    const char* source_file = argv[SOURCE_IMAGE_INDEX];
    const char* destination_file = argv[DESTINATION_IMAGE_INDEX];
    int angle = atoi(argv[ANGLE_INDEX]);

    if (!(angle % 90 == MIDDLE_ANGLE && MIN_ANGLE <= angle && angle <= MAX_ANGLE)) {
        fprintf(stderr, "The angle must be 0, 90, -90, 180, -180, 270, or -270 degrees.\n");
        return READ_ARG_ERROR;
    }

    FILE* input = fopen(source_file, "rb");
    if (!input) {
        perror("Failed to open input file");
        return READ_FILE_OPEN_ERROR;
    }

    struct image img = { 0 };
    enum read_status status_read = from_bmp(input, &img);
    fclose(input);
    if (status_read != READ_OK) {
        fprintf(stderr, "Failed to read the image\n");
        return READ_FAILED;
    }

    struct image rotated_img = rotate_image(&img, angle);
    free_image(&img);

    FILE* output = fopen(destination_file, "wb");
    if (!output) {
        perror("Failed to open output file");
        free_image(&rotated_img);
        return WRITE_FILE_OPEN_ERROR;
    }

    enum write_status status_write = to_bmp(output, &rotated_img);
    fclose(output);
    free_image(&rotated_img);

    if (status_write != WRITE_OK) {
        fprintf(stderr, "Failed to write the image\n");
        return WRITE_ERROR;
    }

    printf("The image was successfully rotated and saved to '%s'.\n", destination_file);
    return WRITE_OK;
}
