#include "bmp.h"
#include "image.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BMP_SIGNATURE 0x4D42
#define BMP_HEADER_SIZE 40
#define DEFAULT_BI_PLANES 1
#define BITS_PER_PIXEL 24
#define NO_COMPRESSION 0

// Вспомогательная функция для вычисления размера строки с учетом padding.
static size_t calculate_row_size(const struct image* img) {
    size_t row_size = img->width * sizeof(struct pixel);
    size_t padding = (4 - (row_size % 4)) % 4;
    return row_size + padding;
}

// Функция для чтения BMP файла.
enum read_status from_bmp(FILE* in, struct image* img) {
    struct bmp_header header;
    if (fread(&header, sizeof(header), 1, in) != 1) {
        return READ_INVALID_HEADER;
    }
    if (header.bfType != BMP_SIGNATURE) {  // 'BM'
        return READ_INVALID_SIGNATURE;
    }

    if (init_image(img, header.biWidth, header.biHeight) != INIT_OK) {
        return READ_FAILED;
    }

    if (fseek(in, header.bOffBits, SEEK_SET) != 0) {
        free(img->data);  // Освобождаем память из-за неудачи fseek
        return READ_FAILED;
    }

    size_t row_size = calculate_row_size(img);
    unsigned char* row = (unsigned char*)malloc(row_size);
    if (!row) {
        free(img->data);  // Освобождаем уже выделенную память для img->data
        return WRITE_ERROR;
    }
    for (size_t i = 0; i < img->height; ++i) {
        if (fread(row, row_size, 1, in) != 1) {
            free(img->data);
            return READ_FAILED;
        }
        memcpy(&img->data[i * img->width], row, img->width * sizeof(struct pixel));
    }
    free(row);

    return READ_OK;
}

// Функция для записи BMP файла.
enum write_status to_bmp(FILE* out, struct image const* img) {
    struct bmp_header header = {0};
    header.bfType = BMP_SIGNATURE;  // 'BM'
    header.bfileSize = sizeof(header) + img->height * calculate_row_size(img);
    header.bOffBits = sizeof(header);
    header.biSize = BMP_HEADER_SIZE;
    header.biWidth = img->width;
    header.biHeight = img->height;
    header.biPlanes = DEFAULT_BI_PLANES;
    header.biBitCount = BITS_PER_PIXEL;
    header.biCompression = NO_COMPRESSION;
    header.biSizeImage = img->height * calculate_row_size(img);

    if (fwrite(&header, sizeof(header), 1, out) != 1) {
        return WRITE_HEADER_ERROR;
    }

    size_t row_size = calculate_row_size(img);
    unsigned char* row = (unsigned char*)malloc(row_size);
    if (!row) {
        free(img->data);  // Освобождаем уже выделенную память для img->data
        return WRITE_ERROR;
    }
    memset(row, 0, row_size);
    for (size_t i = 0; i < img->height; ++i) {
        memcpy(row, &img->data[i * img->width], img->width * sizeof(struct pixel));
        if (fwrite(row, row_size, 1, out) != 1) {
            free(row);
            return WRITE_ERROR;
        }
    }
    free(row);

    return WRITE_OK;
}
