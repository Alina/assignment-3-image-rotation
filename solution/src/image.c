#include "image.h"
#include <stdlib.h>
#include <string.h>

enum init_status init_image(struct image* img, size_t width, size_t height) {
    img->width = width;
    img->height = height;
    img->data = (struct pixel*)malloc(width * height * sizeof(struct pixel));
    if (!img->data) {
        return INIT_FAILED;
    }
    return INIT_OK;
}

// Функция для поворота изображения на 90 градусов.
struct image rotate_image(const struct image* source, int angle) {
    struct image rotated = { 0 };

    if (angle % 180 == 0) {
        rotated.width = source->width;
        rotated.height = source->height;
    } else {
        rotated.width = source->height;
        rotated.height = source->width;
    }
    rotated.data = (struct pixel*)malloc(rotated.width * rotated.height * sizeof(struct pixel));
    if (!rotated.data) {
        return (struct image){0, 0, NULL};
    }

    for (uint64_t y = 0; y < source->height; y++) {
        for (uint64_t x = 0; x < source->width; x++) {
            struct pixel p = source->data[y * source->width + x];
            size_t newX, newY;

            switch (angle) {
                case 90:
                    newX = y;
                    newY = rotated.height - x - 1;
                    break;
                case -270:
                    newX = rotated.width - y - 1;
                    newY = x;
                    break;
                case 180:
                case -180:
                    newX = rotated.width - x - 1;
                    newY = rotated.height - y - 1;
                    break;
                case 270:
                case -90:
                    newX = rotated.width - y - 1;
                    newY = x;
                    break;
                case 0:
                case 360:
                case -360:
                    newX = x;
                    newY = y;
                    break;
                default: // В случае неподдерживаемого угла
                    free(rotated.data);
                    return (struct image){0, 0, NULL};
            }
            rotated.data[newY * rotated.width + newX] = p;
        }
    }
    return rotated;
}


// Функция для освобождения памяти изображения.
void free_image(struct image* img) {
    free(img->data);
    img->data = NULL;
}
