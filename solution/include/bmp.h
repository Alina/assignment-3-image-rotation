#ifndef BMP_H
#define BMP_H

#include <stdint.h>
#include <stdio.h>

// Определение структуры пикселя.
struct pixel {
    uint8_t r, g, b;
};

// Определение структуры изображения.
struct image {
    uint64_t width, height;
    struct pixel* data;
};

// Определение структуры заголовка BMP файла.
#pragma pack(push, 1)
struct bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)

// Перечисления для статусов чтения и записи BMP файла.
// Перечисления для статусов чтения и записи BMP файла.
enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE = 1,
    READ_INVALID_BITS = 2,
    READ_INVALID_HEADER = 3,
    READ_FILE_NOT_FOUND = 4,
    READ_FAILED = 5,
    READ_ARG_ERROR = 6,
    READ_FILE_OPEN_ERROR = 7
};

enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR = -1,
    WRITE_HEADER_ERROR = -2,
    WRITE_FILE_OPEN_ERROR = -3
};

// Функции для работы с BMP файлами.
enum read_status from_bmp(FILE* in, struct image* img);
enum write_status to_bmp(FILE* out, struct image const* img);

#endif
