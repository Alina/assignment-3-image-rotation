#ifndef IMAGE_H
#define IMAGE_H

#include "bmp.h"

enum init_status { INIT_OK, INIT_FAILED };

enum init_status init_image(struct image* img, size_t width, size_t height);
void free_image(struct image* img);
struct image rotate_image(const struct image* source, int angle);

#endif // IMAGE_H
